<?php

namespace App\Console\Commands;

use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;

class ProcessListing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GA:processListings {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process listing from file downloaded';

    protected $indexName;
    const indexType = 'listing';
    protected $indexListing;
    const indexListingType = 'ga_listing';
    protected $client;
    protected $hosts = [];
    protected $count = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->indexName = config('es.index_listing');
        $this->indexListing = config('es.index_ga_listing');
        parent::__construct();
        foreach (config('es.connections.default.servers') as $server) {
            $this->hosts[] = $server['host'] . ':' . $server['port'];
        }
        $this->client = ClientBuilder::create()->setHosts($this->hosts)->build();
    }

    /**
     * Create index
     */
    public function createIndex() {
        // Set the index and type
        $params = [
            'index' => $this->indexListing,
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 0
                ],
                'mappings' => [
                    self::indexListingType => [
                        'properties' => [
                            'Device' => [
                                'type' => 'text'
                            ],
                            'Source' => [
                                'type' => 'text'
                            ],
                            'Medium' => [
                                'type' => 'text'
                            ],
                            'Country' => [
                                'type' => 'text'
                            ],
                            'City' => [
                                'type' => 'text'
                            ],
                            'PagePath' => [
                                'type' => 'text'
                            ],
                            'Date' => [
                                'type' => 'date'
                            ],
                            'Dimension1' => [
                                'type' => 'text'
                            ],
                            'Users' => [
                                'type' => 'integer'
                            ],
                            'PageViews' => [
                                'type' => 'integer'
                            ],
                            'ListingId' => [
                                'type' => 'text'
                            ],
                            'ListingTitle' => [
                                'type' => 'text'
                            ],
                            'ListingType' => [
                                'type' => 'text'
                            ],
                            'Co-Broke' => [
                                'type' => 'text'
                            ],
                            'LsirId' => [
                                'type' => 'text'
                            ]
                        ]
                    ]
                ]
            ]
        ];

        // Create the index with mappings and settings now
        $response = $this->client->indices()->create($params);
        return $response;
    }

    public function getListingById($id){
        $params = [
            'index' => $this->indexName,
            'type' => self::indexType
        ];
        $params['body'] = [
            'query' => [
                'match' => [
                    'WebId' => $id
                ]
            ]
        ];
        $response = $this->client->search($params);
        return $response;
    }

    public function getDate($date){
        $params = [
            'index' => $this->indexListing,
            'type' => self::indexListingType
        ];
        $params['body'] = [
            'query' => [
                'match' => [
                    'Date' => $date
                ]
            ]
        ];
        $response = $this->client->search($params);
        return $response;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ##############################################
        # Create ES index with fields setting
        ##############################################
        //check require create index
        $indexParams['index']  = $this->indexListing;
        if(!$this->client->indices()->exists($indexParams)) {
            $this->createIndex();
        }

        $listings = [];
        $idsNotFound = [];
        $dateDataEmpty = [];
        $dateChecked = [];
        $fileName = $this->argument('file');

        // check file exists
        $checkFileExists = Storage::disk('local')->exists('ga/listings/'.$fileName);
        if (!$checkFileExists) {
            $this->error('File does not exists.');
            return false;
        }

        $this->line('Handle process...');

        // get file's content
        $jsonString = Storage::disk('local')->get('ga/listings/'.$fileName);
        $data = json_decode($jsonString);
        if ($data != NULL) {
            foreach ($data->reports as $rows) {
                foreach($rows as $row){
                    // check date ga exists
                    if (!in_array($row[6], $dateChecked)) {
                        $dateChecked[] = $row[6];
                        $checkDateExists = $this->getDate($row[6]);
                        if (count($checkDateExists['hits']['hits']) == 0) {
                            if (!in_array($row[6], $dateDataEmpty)) {
                                $dateDataEmpty[] = $row[6];
                            }
                        }
                    }

                    if (!in_array($row[6], $dateDataEmpty)) {
                        $this->error('Date '.$row[6].' has been processed.');
                        continue;
                    }

                    // get parameters from url
                    $parts = parse_url($row[5]);
                    $explode = explode('/', $parts['path']); // explode url
                    if(!empty($explode[4])) {
                        $listingId = end($explode);
                        $coBrokeID = '';
                        $lsirId = '';

                        if (!empty($parts['query'])) {
                            parse_str($parts['query'], $parameters);
                            $coBrokeID = !empty($parameters['agentid']) ? $parameters['agentid'] : $coBrokeID;
                            $lsirId = !empty($parameters['lsirid']) ? $parameters['lsirid'] : $lsirId;
                        }

                        //
                        if (!key_exists($listingId, $listings) && !in_array($listingId, $idsNotFound)) {
                            $listings[$listingId] = $this->getListingById($listingId);
                        }
                        $result = $listings[$listingId];

                        $title = '';
                        $type = '';
                        $agentIds = [];
                        if (isset($result['hits']['hits'][0])) {
                            $listing = $result['hits']['hits'][0]['_source'];
                            $title = $listing['Title'];
                            $type = $listing['Price']['SaleORrental'] == 'S' ? 'Sale' : 'Rent';
                            if (empty($listing['Agents']['Agent']['ID'])) {
                                foreach ($listing['Agents']['Agent'] as $agent) {
                                    $agentIds[] = $agent['ID'];
                                }
                            } else {
                                $agentIds[] = $listing['Agents']['Agent']['ID'];
                            }
                        } else {
                            // get listing ids not found in ES
                            if(!in_array($listingId, $idsNotFound)){
                                $idsNotFound[] = $listingId;
                                $this->line('Not found listing: ' . $listingId);
                            }
                        }

                        $arrayFieldOld = [
                            'Device' => $row[0],
                            'Source' => $row[1],
                            'Medium' => $row[2],
                            'Country' => $row[3],
                            'City' => $row[4],
                            'PagePath' => $row[5],
                            'Date' => $row[6],
                            'Dimension1' => $row[7],
                            'Users' => $row[8],
                            'PageViews' => $row[9]
                        ];
                        $arrayFieldNew = [
                            'ListingId' => $listingId,
                            'ListingTitle' => $title,
                            'ListingType' => $type,
                            'AgentIds' => $agentIds,
                            'Co-Broke' => $coBrokeID,
                            'LsirId' => $lsirId
                        ];

                        $listingsResult = array_merge($arrayFieldOld, $arrayFieldNew);

                        $params = [];
                        $params['index'] = $this->indexListing;
                        $params['type']  = self::indexListingType;

                        $params['body']  = $listingsResult;
                        $result = $this->client->index($params); //using Index() function to inject the data
                        $this->line('Create ' . $result['_id']);
                        $this->count++;
                    }
                }
            }
        }
        $this->line('Total: '.$this->count);
        $this->line('End processing...');
    }
}
