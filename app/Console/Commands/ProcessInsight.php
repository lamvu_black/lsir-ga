<?php

namespace App\Console\Commands;

use Elasticsearch\ClientBuilder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ProcessInsight extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GA:processInsights {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process insight from file download.';

    protected $indexName;
    protected $indexInsight;
    const indexType = 'post';
    const indexInsightType = 'ga_insight';
    protected $client;
    protected $hosts = [];
    protected $count = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->indexName = config('es.index_wp');
        $this->indexInsight = config('es.index_ga_insight');
        parent::__construct();
        foreach (config('es.connections.default.servers') as $server) {
            $this->hosts[] = $server['host'] . ':' . $server['port'];
        }
        $this->client = ClientBuilder::create()->setHosts($this->hosts)->build();
    }

    /**
     * Create index
     */
    public function createIndex() {
        // Set the index and type
        $params = [
            'index' => $this->indexInsight,
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 0
                ],
                'mappings' => [
                    self::indexInsightType => [
                        'properties' => [
                            'Device' => [
                                'type' => 'text'
                            ],
                            'Source' => [
                                'type' => 'text'
                            ],
                            'Medium' => [
                                'type' => 'text'
                            ],
                            'Country' => [
                                'type' => 'text'
                            ],
                            'City' => [
                                'type' => 'text'
                            ],
                            'PagePath' => [
                                'type' => 'text'
                            ],
                            'Date' => [
                                'type' => 'date'
                            ],
                            'Dimension1' => [
                                'type' => 'text'
                            ],
                            'Users' => [
                                'type' => 'integer'
                            ],
                            'PageViews' => [
                                'type' => 'integer'
                            ],
                            'InsightId' => [
                                'type' => 'text'
                            ],
                            'InsightTitle' => [
                                'type' => 'text'
                            ],
                            'LsirId' => [
                                'type' => 'text'
                            ]
                        ]
                    ]
                ]
            ]
        ];

        // Create the index with mappings and settings now
        $response = $this->client->indices()->create($params);
        return $response;
    }

    public function getInsightById($id){
        $params = [
            'index' => $this->indexName,
            'type' => self::indexType
        ];
        $params['body'] = [
            'query' => [
                'match' => [
                    'ID' => $id
                ]
            ]
        ];
        $response = $this->client->search($params);
        return $response;
    }

    public function getDate($date){
        $params = [
            'index' => $this->indexInsight,
            'type' => self::indexInsightType
        ];
        $params['body'] = [
            'query' => [
                'match' => [
                    'Date' => $date
                ]
            ]
        ];
        $response = $this->client->search($params);
        return $response;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ##############################################
        # Create ES index with fields setting
        ##############################################
        //check require create index
        $indexParams['index']  = $this->indexInsight;
        if(!$this->client->indices()->exists($indexParams)) {
            $this->createIndex();
        }

        $insights = [];
        $idsNotFound = [];
        $fileName = $this->argument('file');

        // check file exists
        $checkFileExists = Storage::disk('local')->exists('ga/insights/'.$fileName);
        if (!$checkFileExists) {
            $this->error('File does not exists.');
            return false;
        }

        $this->line('Handle process...');

        $jsonString = Storage::disk('local')->get('ga/insights/'.$fileName);
        $data = json_decode($jsonString);
        $dateDataEmpty = [];
        $dateChecked = [];
        if ($data != NULL) {
            foreach ($data->reports as $rows) {
                foreach ($rows as $row) {
                    // check date ga exists
                    if (!in_array($row[6], $dateChecked)) {
                        $dateChecked[] = $row[6];
                        $checkDateExists = $this->getDate($row[6]);
                        if (count($checkDateExists['hits']['hits']) == 0) {
                            if (!in_array($row[6], $dateDataEmpty)) {
                                $dateDataEmpty[] = $row[6];
                            }
                        }
                    }

                    if (!in_array($row[6], $dateDataEmpty)) {
                        $this->error('Date ' . $row[6] . ' has been processed.');
                        continue;
                    }

                    // get parameters from pagePath
                    $parts = parse_url($row[5]);
                    $explode = explode('/', $parts['path']); // explode url

                    if (!empty($explode[3]) && is_numeric($explode[3])) {
                        $insightId = !empty($explode[3]) ? $explode[3] : '';
                        $lsirId = '';
                        $title = '';
                        if (!empty($parts['query'])) {
                            parse_str($parts['query'], $parameters);
                            $lsirId = !empty($parameters['lsirid']) ? $parameters['lsirid'] : $lsirId;
                        }

                        if (!key_exists($insightId, $insights)) {
                            $insights[$insightId] = $this->getInsightById($insightId);
                        }
                        $result = $insights[$insightId];

                        if (isset($result['hits']['hits'][0])) {
                            $insight = $result['hits']['hits'][0]['_source'];
                            $title = $insight['post_title'];
                        } else {
                            // get listing ids not found in ES
                            if(!in_array($insightId, $idsNotFound)){
                                $idsNotFound[] = $insightId;
                                $this->line('Insight id not found: ' . $insightId);
                            }
                        }

                        $arrayFieldOld = [
                            'Device' => $row[0],
                            'Source' => $row[1],
                            'Medium' => $row[2],
                            'Country' => $row[3],
                            'City' => $row[4],
                            'PagePath' => $row[5],
                            'Date' => $row[6],
                            'Dimension1' => $row[7],
                            'Users' => $row[8],
                            'PageViews' => $row[9]
                        ];
                        $arrayFieldNew = [
                            'InsightId' => $insightId,
                            'InsightTitle' => $title,
                            'LsirId' => $lsirId
                        ];
                        $insightsResult = array_merge($arrayFieldOld, $arrayFieldNew);
                        // insert to ES
                        $params = [];
                        $params['index'] = $this->indexInsight;
                        $params['type']  = self::indexInsightType;

                        $params['body']  = $insightsResult;
                        $result = $this->client->index($params); //using Index() function to inject the data
                        $this->line('Create ' . $result['_id']);
                        $this->count++;
                    }
                }
            }
        }
        $this->line('Total: '.$this->count);
        $this->line('End processing...');
    }
}
