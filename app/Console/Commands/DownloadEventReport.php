<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Google_Client;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_GetReportsRequest;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DownloadEventReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GA:eventReportDownload {startDate} {endDate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download reports event from google analytics';

    const APP_NAME = "Page Analytics Reporting";
    const VIEW_ID = "152606061";
    const SCOPES = ['https://www.googleapis.com/auth/analytics.readonly'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function initializeAnalytics()
    {

        // Use the developers console and download your service account
        // credentials in JSON format. Place them in this directory or
        // change the key file location if necessary.
        $KEY_FILE_LOCATION = base_path('ga/service-acc-Agent-oAuth2-f6dc00fd60da.json');

        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName(self::APP_NAME);
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(self::SCOPES);
        $analytics = new Google_Service_AnalyticsReporting($client);

        return $analytics;
    }

    public function getReport($startDate, $endDate, $pageToken) {
        $analytics = $this->initializeAnalytics();
        $pageSize = "30";

        $query = [
            "viewId" => self::VIEW_ID,
            "dateRanges" => [
                "startDate" => $startDate,
                "endDate" => $endDate
            ],
            "metrics" => [
                ["expression" => "ga:users"],
                ["expression" => "ga:pageviews"]
            ],
            "dimensions" => [
                ["name" => "ga:source"],
                ["name" => "ga:medium"],
                ["name" => "ga:country"],
                ["name" => "ga:pagePath"],
                ["name" => "ga:eventCategory"],
                ["name" => "ga:eventAction"],
                ["name" => "ga:eventLabel"],
                ["name" => "ga:date"]
            ],
            "dimensionFilterClauses" => [
                'filters' => [
                    "dimension_name" => "ga:eventCategory",
                    "operator" => "PARTIAL", // valid operators can be found here: https://developers.google.com/analytics/devguides/reporting/core/v4/rest/v4/reports/batchGet#FilterLogicalOperator
                    "expressions" => "Lead Gen"
                ]
            ],
            "samplingLevel" => "LARGE",
            "includeEmptyRows" => "false",
            "pageSize" => $pageSize,
            "pageToken" => $pageToken,
            "orderBys" => [
                "fieldName"=> "ga:date",
                "sortOrder"=> "DESCENDING"
            ]
        ];

        // build the request and response
        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests(array($query));
        // now batchGet the results https://developers.google.com/analytics/devguides/reporting/core/v4/rest/v4/reports/batchGet
        $report = $analytics->reports->batchGet($body);
        return $report;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startDate = $this->argument('startDate');
        $endDate = $this->argument('endDate');
        // convert date to Y-m-d
        $startDateString = Carbon::parse($startDate)->toDateString();
        $endDateString = Carbon::parse($endDate)->toDateString();
        $today = Carbon::parse(Carbon::now());
        $minDate = Carbon::parse($endDate)->min(Carbon::parse($startDate));

        // dont download reporting when start date or end date is today
        if ($today->isSameDay(Carbon::parse($startDate)) || $today->isSameDay(Carbon::parse($endDate))) {
            $this->error('Start Date and End Date must be smaller today.');
            return false;
        }

        // start date must be smaller end date
        if (!$minDate->isSameDay(Carbon::parse($startDate))) {
            $this->error('Start date must be smaller End date');
            return false;
        }

        $this->line('Downloading...');
        $reports = [];
        // loop until nextPageToken == ''
        do {
            $nextPageToken = count($reports) ? $reports[count($reports) - 1]->getNextPageToken() : null;
            $response = $this->getReport($startDateString, $endDateString, $nextPageToken);
            $reports[] = $response[0];
        } while ($response[0]->getNextPageToken() != '');

        $eventResult = [];
        foreach ($reports as $report) {
            $rows = $report->getData()->getRows();
            foreach ($rows as $row) {
                // get users and pageViews, merge them to dimensions
                $metrics = $row->getMetrics();
                $arrayMetric = $metrics[0]->getValues();
                $eventFields = array_merge($row->getDimensions(), $arrayMetric);

                $eventResult['reports']['dimensions'][] = $eventFields;
                $eventResult['time'] = $startDateString.'_'.$endDateString;
            }
        }

        Storage::disk('local')->put('ga/events/Download-'.$startDateString.'_'.$endDateString.'.json', json_encode($eventResult, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
        $this->info('Download complete!');
    }
}
