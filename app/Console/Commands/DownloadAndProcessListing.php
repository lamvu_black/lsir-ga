<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class DownloadAndProcessListing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GA:listingDownloadAndProcess {startDate} {endDate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle download and process listing reporting';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startDate = $this->argument('startDate');
        $endDate = $this->argument('endDate');
        $download = Artisan::call('GA:listingReportDownload', ['startDate' => $startDate, 'endDate' => $endDate]);
        if ($download == 0) {
            Artisan::call('GA:processListing');
            $this->line(Artisan::output());
        } else {
            $this->line('Error! Download failed.');
        }
    }
}
