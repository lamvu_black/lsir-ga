<?php

namespace App\Console\Commands;

use Elasticsearch\ClientBuilder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ProcessEvent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GA:processEvents {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process event from file download';

    const indexAgent = 'lsir_agent';
    const indexTypeAgent = 'agent';
    const indexListing = 'lsir_listing';
    const indexTypeListing = 'listing';
    protected $indexEvent;
    const indexEventContactType = 'contact';
    const indexEventEnquiryType = 'enquiry';
    protected $client;
    protected $hosts = [];
    protected $count = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->indexEvent = config('es.index_ga_event');
        parent::__construct();
        foreach (config('es.connections.default.servers') as $server) {
            $this->hosts[] = $server['host'] . ':' . $server['port'];
        }
        $this->client = ClientBuilder::create()->setHosts($this->hosts)->build();
    }

    /**
     * Create index
     */
    public function createIndex() {
        // Set the index and type
        $params = [
            'index' => $this->indexEvent,
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 0
                ],
                'mappings' => [
                    self::indexEventEnquiryType => [
                        'properties' => [
                            'Source' => [
                                'type' => 'text'
                            ],
                            'Medium' => [
                                'type' => 'text'
                            ],
                            'Country' => [
                                'type' => 'text'
                            ],
                            'PagePath' => [
                                'type' => 'text'
                            ],
                            'EventCategory' => [
                                'type' => 'text'
                            ],
                            'EventAction' => [
                                'type' => 'text'
                            ],
                            'EventLabel' => [
                                'type' => 'text'
                            ],
                            'Date' => [
                                'type' => 'date'
                            ],
                            'Users' => [
                                'type' => 'integer'
                            ],
                            'PageViews' => [
                                'type' => 'integer'
                            ],
                            'Type' => [
                                'type' => 'text'
                            ],
                            'SubType' => [
                                'type' => 'text'
                            ],
                            'LsirId' => [
                                'type' => 'text'
                            ]
                        ]
                    ],
                    self::indexEventContactType => [
                        'properties' => [
                            'Source' => [
                                'type' => 'text'
                            ],
                            'Medium' => [
                                'type' => 'text'
                            ],
                            'Country' => [
                                'type' => 'text'
                            ],
                            'PagePath' => [
                                'type' => 'text'
                            ],
                            'EventCategory' => [
                                'type' => 'text'
                            ],
                            'EventAction' => [
                                'type' => 'text'
                            ],
                            'EventLabel' => [
                                'type' => 'text'
                            ],
                            'Date' => [
                                'type' => 'date'
                            ],
                            'Users' => [
                                'type' => 'integer'
                            ],
                            'PageViews' => [
                                'type' => 'integer'
                            ],
                            'Type' => [
                                'type' => 'text'
                            ],
                            'SubType' => [
                                'type' => 'text'
                            ],
                            'LsirId' => [
                                'type' => 'text'
                            ]
                        ]
                    ]
                ]
            ]
        ];
        // Create the index with mappings and settings now
        $response = $this->client->indices()->create($params);
        return $response;
    }

    public function getAgentByEmail($email){
        $params = [
            'index' => self::indexAgent,
            'type' => self::indexTypeAgent
        ];

        $params['body'] = [
            'query' => [
                'bool' => [
                    'should' => [
                        [
                            'match' => [
                                'AgentEmail1.keyword' => $email
                            ]
                        ],
                        [
                            'match' => [
                                'AgentEmail2.keyword' => $email
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $response = $this->client->search($params);
        return $response;
    }

    public function getAgentByEventLabel($eventLabel){
        $array = explode(':', $eventLabel);
        $phoneNumber = '+'.str_replace('-', ' ', $array[1]);

        $params = [
            'index' => self::indexAgent,
            'type' => self::indexTypeAgent
        ];
        $params['body'] = [
            'query' => [
                'match' => [
                    'AgentPhonePrimary.keyword' => $phoneNumber
                ]
            ]
        ];
        $response = $this->client->search($params);
        return $response;
    }

    public function getListingById($id){
        $params = [
            'index' => self::indexListing,
            'type' => self::indexTypeListing
        ];
        $params['body'] = [
            'query' => [
                'match' => [
                    'WebId' => $id
                ]
            ]
        ];
        $response = $this->client->search($params);
        return $response;
    }

    public function getDate($date){
        $params = [
            'index' => $this->indexEvent,
            'type' => [self::indexEventContactType, self::indexEventEnquiryType]
        ];
        $params['body'] = [
            'query' => [
                'match' => [
                    'Date' => $date
                ]
            ]
        ];
        $response = $this->client->search($params);
        return $response;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ##############################################
        # Create ES index with fields setting
        ##############################################
        //check require create index
        $indexParams['index']  = $this->indexEvent;
        if(!$this->client->indices()->exists($indexParams)) {
            $this->createIndex();
        }

        $listings = [];
        $arrayFieldNew = [];
        $dateDataEmpty = [];
        $dateChecked = [];
        $fileName = $this->argument('file');

        // check file exists
        $checkFileExists = Storage::disk('local')->exists('ga/events/'.$fileName);
        if (!$checkFileExists) {
            $this->error('File does not exists.');
            return false;
        }

        $this->line('Handle process...');

        $jsonFile = Storage::disk('local')->get('ga/events/'.$fileName);
        $data = json_decode($jsonFile);
        if ($data != NULL) {
            foreach ($data ->reports as $rows) {
                foreach ($rows as $row) {
                    // check date ga exists
                    if (!in_array($row[7], $dateChecked)) {
                        $dateChecked[] = $row[7];
                        $checkDateExists = $this->getDate($row[7]);
                        if (count($checkDateExists['hits']['hits']) == 0) {
                            if (!in_array($row[7], $dateDataEmpty)) {
                                $dateDataEmpty[] = $row[7];
                            }
                        }
                    }

                    if (!in_array($row[7], $dateDataEmpty)) {
                        $this->error('Date '.$row[7].' has been processed.');
                        continue;
                    }

                    $marketingAgentIds = [];
                    $lsirId = '';
                    $coBrokers = [];
                    $enquiryType = 'Enquiry';
                    $contactType = 'Contact';

                    // ignore eventAction == Enquiry Click
                    if($row[5] !== 'Enquiry Click') {
                        $parts = parse_url($row[3]);
                        $explode = explode('/', $parts['path']); // explode url
                        $listingId = !empty($explode[5]) ? $explode[5] : '';

                        if (!key_exists($listingId, $listings)) {
                            $listings[$listingId] = $this->getListingById($listingId);
                        }
                        $result = $listings[$listingId];

                        // check listing exists
                        if (isset($result['hits']['hits'][0])) {
                            $listing = $result['hits']['hits'][0]['_source'];
                            // get Agent Marketing ID/s
                            if (empty($listing['Agents']['Agent']['ID'])) {
                                foreach ($listing['Agents']['Agent'] as $agent) {
                                    $marketingAgentIds[] = $agent['ID'];
                                }
                            } else {
                                $marketingAgentIds[] = $listing['Agents']['Agent']['ID'];
                            }
                        }

                        // get LSIR ID
                        if (!empty($parts['query'])) {
                            parse_str($parts['query'], $parameters);
                            $lsirId = !empty($parameters['lsirid']) ? $parameters['lsirid'] : $lsirId;
                        }

                        // For event action: Contact Us, Market Your Property, Free Property Tour
                        $events = ['Contact Us', 'Market-your-property', 'Free-property-tour'];
                        if (in_array($row[5], $events)) {
                            // get sub type for event
                            $subType = $row[5] == 'Contact Us' ? 'Contact Us' : ($row[5] == 'Market-your-property' ? 'Market Your Property' : ($row[5] == 'Free-property-tour' ? 'Property Tour' : ''));
                            $arrayFieldNew = [
                                'Type' => $enquiryType,
                                'SubType' => $subType,
                                'LsirId' => $lsirId
                            ];
                        }

                        // For Enquiry(Agent)
                        $numberSimilarEnquiryAgent = similar_text($row[5], 'Enquiry - (Agent)', $percentEnquiryAgent);
                        // check percent similar between eventAction with 'string' must be larger 50%
                        // and number character == character 'string'
                        if ($percentEnquiryAgent > 50 && $numberSimilarEnquiryAgent == strlen('Enquiry - (Agent)')) {
                            $agentId = '';
                            $subType = 'Enquiry(Agent)';
                            $emails = explode('|', $row[6]);
                            $agent = $this->getAgentByEmail($emails[0]);
                            if (isset($agent['hits']['hits'][0])) {
                                $agentId = $agent['hits']['hits'][0]['_source']['AgentID'];
                            }
                            $arrayFieldNew = [
                                'Type' => $enquiryType,
                                'SubType' => $subType,
                                'AgentId' => $agentId,
                                'LsirId' => $lsirId
                            ];
                        }

                        // For Enquiry(Property)
                        $numberSimilarPropertyEnquiry = similar_text($row[5], 'Property-enquiry', $percentPropertyEnquiry);
                        if ($percentPropertyEnquiry > 50 && $numberSimilarPropertyEnquiry == strlen('Property-enquiry')) {
                            $subType = 'Enquiry(Property)';
                            $emails = explode('|', $row[6]);
                            // get all agent id from eventLabel's email
                            foreach ($emails as $email) {
                                $agent = $this->getAgentByEmail($email);
                                if (isset($agent['hits']['hits'][0])) {
                                    $agentId = $agent['hits']['hits'][0]['_source']['AgentID'];
                                    // compare agentId vs marketingIds
                                    if (!in_array($agentId, $marketingAgentIds)) {
                                        $coBrokers[] = $agentId;
                                    }
                                }
                            }

                            $arrayFieldNew = [
                                'Type' => $enquiryType,
                                'SubType' => $subType,
                                'ListingId' => $listingId,
                                'MarketingAgents' => $marketingAgentIds,
                                'Co-Broke' => $coBrokers,
                                'LsirId' => $lsirId
                            ];
                        }

                        // For WhatsApp(Property) and Call Agent(Property)
                        if ($row[5] == 'WhatsApp Agent' || $row[5] == 'Contact Agent') {
                            $subType = $row[5] == 'WhatsApp Agent' ? 'WhatsApp(Property)' : ($row[5] == 'Contact Agent' ? 'Call Agent(Property)' : '');
                            $coBroker = '';

                            $agent = $this->getAgentByEventLabel($row[6]);
                            if (isset($agent['hits']['hits'][0])) {
                                $agentId = $agent['hits']['hits'][0]['_source']['AgentID'];
                                if (!in_array($agentId, $marketingAgentIds)) {
                                    $coBroker = $agentId;
                                }
                            }
                            $arrayFieldNew = [
                                'Type' => $contactType,
                                'SubType' => $subType,
                                'ListingId' => $listingId,
                                'MarketingAgents' => $marketingAgentIds,
                                'Co-Broke' => $coBroker,
                                'LsirId' => $lsirId
                            ];
                        }

                        // For WhatsApp(Agent) and Call Agent(Agent) and Call Office(Agent)
                        if ($row[5] == 'WhatsApp Agent(A)' || $row[5] == 'Contact Agent(A)' || $row[5] == 'Contact Office(A)') {
                            $agentId = '';
                            $subType = $row[5] == 'WhatsApp Agent(A)' ? 'WhatsApp(Agent)' : ($row[5] == 'Contact Agent(A)' ? 'Call Agent(Agent)' : ($row[5] == 'Contact Office(A)' ? 'Call Office(Agent)' : ''));

                            $agent = $this->getAgentByEventLabel($row[6]);
                            if (isset($agent['hits']['hits'][0])) {
                                $agentId = $agent['hits']['hits'][0]['_source']['AgentID'];
                            }

                            $arrayFieldNew = [
                                'Type' => $contactType,
                                'SubType' => $subType,
                                'AgentId' => $agentId,
                                'LsirId' => $lsirId
                            ];
                        }

                        $arrayFieldOld = [
                            'Source' => $row[0],
                            'Medium' => $row[1],
                            'Country' => $row[2],
                            'PagePath' => $row[3],
                            'EventCategory' => $row[4],
                            'EventAction' => $row[5],
                            'EventLabel' => $row[6],
                            'Date' => $row[7],
                            'Users' => $row[8],
                            'PageViews' => $row[9]
                        ];
                        $eventsResult = array_merge($arrayFieldOld, $arrayFieldNew);

                        $params = [];
                        $params['index'] = $this->indexEvent;
                        if ($eventsResult['Type'] == $contactType) {
                            $params['type']  = self::indexEventContactType;
                        } else {
                            $params['type']  = self::indexEventEnquiryType;
                        }

                        $params['body']  = $eventsResult;
                        $result = $this->client->index($params); //using Index() function to inject the data
                        $this->line('Create ' . $result['_id']);
                        $this->count++;
                    }
                }
            }
            $this->line('Total: '.$this->count);
        }
        $this->line('End Processing...!');
    }
}
